# BasketTDD

Customer basket that allows a customer to add products and provides a total cost of the
basket including applicable discounts.

## Available products

Product | Cost
--- | --- 
Butter | £0.80
Milk | £1.15
Bread | £1.00

## Offers

* Buy 2 Butter and get a Bread at 50% off
* Buy 3 Milk and get the 4th milk for free