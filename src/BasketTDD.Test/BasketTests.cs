﻿using BasketTDD.Domain;
using BasketTDD.Domain.Offers;
using BasketTDD.Domain.Products;
using System;
using System.Linq;
using Xunit;

namespace BasketTDD.Test
{
    public class BasketTests
    {
        [Fact]
        public void Basket_1Bread_1Butter_1Milk_Total_2Pounds95()
        {
            var basket = new Basket();
            basket.AddProducts(new Bread(), new Milk(), new Butter());

            var total = basket.CalculateTotal();

            Assert.Equal(expected: 2.95m, actual: total);
        }

        [Fact]
        public void Basket_AddProducts_Actually_Adds_Products()
        {
            var basket = new Basket();
            var butter = new Butter();

            basket.AddProducts(butter);

            Assert.Equal(expected: butter, actual: basket.Products[0]);
        }

        [Fact]
        public void Basket_AddProducts_Null_Throws_ArgumentNullException()
        {
            var basket = new Basket();

            var exception = Assert.Throws<ArgumentNullException>(() => basket.AddProducts(products: null));
            Assert.StartsWith(
                expectedStartString: "Products parameter cannot be null",
                actualString: exception.Message);
        }

        [Fact]
        public void Basket_AddProducts_Array_With_Null_Throws_ArgumentNullException()
        {
            var basket = new Basket();

            var exception = Assert.Throws<ArgumentNullException>(() => basket.AddProducts(products: new Product[] { null }));
            Assert.StartsWith(
                expectedStartString: "Products parameter cannot contain nulls",
                actualString: exception.Message);
        }

        [Fact]
        public void Basket_2Butter_2Bread_Total_3Pounds10()
        {
            var basket = new Basket();
            basket.AddProducts(
                new Bread(),
                new Bread(),
                new Butter(),
                new Butter()
            )
            .WithOffers(new BreadOffer());

            var total = basket.CalculateTotal();

            Assert.Equal(expected: 3.1m, actual: total);
        }

        [Fact]
        public void Basket_4Milk_Total_3Pounds45()
        {
            var basket = new Basket();
            basket.AddProducts(
                Enumerable.Repeat(
                    element: new Milk(),
                    count: 4)
                .ToArray()
            )
            .WithOffers(new MilkOffer());

            var total = basket.CalculateTotal();

            Assert.Equal(expected: 3.45m, actual: total);
        }

        [Fact]
        public void Basket_WithOffers_Null_Throws_ArgumentNullException()
        {
            var basket = new Basket();

            var exception = Assert.Throws<ArgumentNullException>(() => basket.WithOffers(offers: null));
            Assert.StartsWith(
                expectedStartString: "Offers parameter cannot be null",
                actualString: exception.Message);
        }

        [Fact]
        public void Basket_WithOffers_Array_With_Null_Throws_ArgumentNullException()
        {
            var basket = new Basket();

            var exception = Assert.Throws<ArgumentNullException>(() => basket.WithOffers(offers: new Offer[] { null }));
            Assert.StartsWith(
                expectedStartString: "Offers parameter cannot contain nulls",
                actualString: exception.Message);
        }

        [Fact]
        public void Basket_2Butter_1Bread_8Milk_Total_9Pounds()
        {
            var basket = new Basket();
            var products = Enumerable
                .Repeat<Product>(element: new Milk(), count: 8)
                .ToList();
            products.AddRange(new Product[]
            {
                new Butter(),
                new Butter(),
                new Bread()
            });
            basket
                .AddProducts(products.ToArray())
                .WithOffers(new MilkOffer(), new BreadOffer());

            var total = basket.CalculateTotal();

            Assert.Equal(9m, total);
        }

        [Fact]
        public void Basket_Same_Offers_DoNot_Cumulate()
        {
            var basket = new Basket();
            basket
                .AddProducts(products: Enumerable.Repeat<Product>(new Milk(), 4).ToArray())
                .WithOffers(new MilkOffer(), new MilkOffer());

            var total = basket.CalculateTotal();

            Assert.Equal(expected: 3.45m, actual: total);
        }
    }
}
