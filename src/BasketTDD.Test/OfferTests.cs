﻿using BasketTDD.Domain.Offers;
using BasketTDD.Domain.Products;
using System.Linq;
using Xunit;

namespace BasketTDD.Test
{
    public class OfferTests
    {
        [Fact]
        public void BreadOffer_50PercentOff_When_2Butter()
        {
            var breadOffer = new BreadOffer();
            var products = new Product[]
            {
                new Butter(),
                new Butter(),
                new Bread()
            };

            var discount = breadOffer.GetDiscountCalculator()(products);

            Assert.Equal(expected: 0.5m, actual: discount);
        }

        [Fact]
        public void MilkOffer_3Milks_Fourth_Free()
        {
            var milkOffer = new MilkOffer();
            var products = Enumerable.Repeat(element: new Milk(), count: 4);

            var discount = milkOffer.GetDiscountCalculator()(products);

            Assert.Equal(expected: 1.15m, actual: discount);
        }
    }
}
