﻿using BasketTDD.Domain.Products;
using Xunit;

namespace BasketTDD.Test
{
    public class ProductTests
    {
        [Fact]
        public void Butter_Has_Correct_Name_And_Cost()
        {
            var butter = new Butter();

            Assert.Equal(expected: "Butter", actual: butter.Name);
            Assert.Equal(expected: 0.8m, actual: butter.Cost);
        }

        [Fact]
        public void Milk_Has_Correct_Name_And_Cost()
        {
            var milk = new Milk();

            Assert.Equal(expected: "Milk", actual: milk.Name);
            Assert.Equal(expected: 1.15m, actual: milk.Cost);
        }

        [Fact]
        public void Bread_Has_Correct_Name_And_Cost()
        {
            var bread = new Bread();

            Assert.Equal(expected: "Bread", actual: bread.Name);
            Assert.Equal(expected: 1m, actual: bread.Cost);
        }
    }
}
