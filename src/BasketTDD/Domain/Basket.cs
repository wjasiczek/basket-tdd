﻿using BasketTDD.Domain.Offers;
using BasketTDD.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BasketTDD.Domain
{
    public class Basket
    {
        private readonly List<Product> _products = new List<Product>();
        public IReadOnlyList<Product> Products => _products.AsReadOnly();
        private readonly HashSet<Offer> _offers = new HashSet<Offer>();

        public Basket()
        {
        }

        public decimal CalculateTotal()
        {
            var total = _products.Sum(product => product.Cost);

            return total - CalculateDiscountFromOffers();
        }

        public Basket AddProducts(params Product[] products)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products), "Products parameter cannot be null");

            if (products.Any(product => product == null))
                throw new ArgumentNullException(nameof(products), "Products parameter cannot contain nulls");

            _products.AddRange(products);

            return this;
        }

        public Basket WithOffers(params Offer[] offers)
        {
            if (offers == null)
                throw new ArgumentNullException(nameof(offers), "Offers parameter cannot be null");

            if (offers.Any(offer => offer == null))
                throw new ArgumentNullException(nameof(offers), "Offers parameter cannot contain nulls");

            foreach (var offer in offers)
                _offers.Add(offer);

            return this;
        }

        private decimal CalculateDiscountFromOffers()
        {
            var discounts = 0m;
            foreach (var offer in _offers)
                discounts += offer.GetDiscountCalculator()(Products);

            return discounts;
        }
    }
}