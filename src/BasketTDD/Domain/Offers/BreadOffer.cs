﻿using BasketTDD.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BasketTDD.Domain.Offers
{
    public class BreadOffer : Offer, IEquatable<BreadOffer>
    {
        public override Func<IEnumerable<Product>, decimal> GetDiscountCalculator() =>
            products =>
            {
                var numberOfButters = products.Count(product => product is Butter);
                var numberOfDiscounts = numberOfButters / 2;
                var numberOfBreads = products.Count(product => product is Bread);
                var breadPrice = products.FirstOrDefault(product => product is Bread)?.Cost;

                return Math.Min(numberOfDiscounts, numberOfBreads) * breadPrice / 2 ?? 0;
            };

        public override bool Equals(object obj) => (obj ?? false) is BreadOffer breadOffer;

        public bool Equals(BreadOffer breadOffer) => breadOffer != null;

        public override int GetHashCode() => GetType().AssemblyQualifiedName.GetHashCode();
    };
}

