﻿using BasketTDD.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BasketTDD.Domain.Offers
{
    public class MilkOffer : Offer, IEquatable<MilkOffer>
    {
        public override Func<IEnumerable<Product>, decimal> GetDiscountCalculator() =>
            products =>
            {
                var numberOfMilks = products.Count(product => product is Milk);
                var numberOfDiscounts = numberOfMilks / 4;
                var milkPrice = products.FirstOrDefault(product => product is Milk)?.Cost;

                return numberOfDiscounts * milkPrice ?? 0;
            };

        public override bool Equals(object obj) => (obj ?? false) is MilkOffer milkOffer;

        public bool Equals(MilkOffer milkOffer) => milkOffer != null;

        public override int GetHashCode() => GetType().AssemblyQualifiedName.GetHashCode();
    }
}