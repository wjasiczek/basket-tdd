﻿using BasketTDD.Domain.Products;
using System;
using System.Collections.Generic;

namespace BasketTDD.Domain.Offers
{
    public abstract class Offer
    {
        public abstract Func<IEnumerable<Product>, decimal> GetDiscountCalculator();
    }
}
