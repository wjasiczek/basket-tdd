﻿namespace BasketTDD.Domain.Products
{
    public class Bread : Product
    {
        public Bread() : base("Bread", 1m)
        {
        }
    }
}