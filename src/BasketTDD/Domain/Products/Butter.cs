﻿namespace BasketTDD.Domain.Products
{
    public class Butter : Product
    {
        public Butter() : base("Butter", 0.8m)
        {
        }
    }
}