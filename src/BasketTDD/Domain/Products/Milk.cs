﻿namespace BasketTDD.Domain.Products
{
    public class Milk : Product
    {
        public Milk() : base("Milk", 1.15m)
        {
        }
    }
}